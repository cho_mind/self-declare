const functions = require("firebase-functions");
const request = require("request-promise");
const admin = require("firebase-admin");
const moment = require("moment");
const { get } = require("lodash")

const region = 'asia-southeast2';
const DB_COLLECTION = 'users'
const DB_LASTUPDATE = 'lastUpdate'
const DB_TEAM = 'team'

const LINE_MESSAGING_API = 'https://api.line.me/v2/bot/message';
const LINE_HEADER = {
  'Content-Type': 'application/json',
  'Authorization': `Bearer ukXIvxiYqV8kgrtQLxRrZROCPgIf2+pl4zv6quyoGUfyv+IpqhGKowtyjryIfMHCRDSOcQznp/mbOBFj749CWK6SI9GUke38pyO5+ejbYak4S4+hj+GAcuLG0m1O8CAQPGz37jtp39Xp+BlYRFNdlwdB04t89/1O/w1cDnyilFU=`
};

admin.initializeApp({
  credential: admin.credential.applicationDefault()
});

const db = admin.firestore();
// quickstartAddData(db);

const updateData = async ({ id, params }) => {
  const updater = db.collection(DB_COLLECTION).doc(id);
  return await updater.set(params);
}

const updateDate = async (params) => {
  const updateDateRef = db.collection(DB_LASTUPDATE).doc(DB_LASTUPDATE);
  return await updateDateRef.set(params);
}

const getDataAll = async () => {
  return await db.collection(DB_COLLECTION).get();
}

const getTeamAll = async () => {
  return await db.collection(DB_TEAM).get();
}

const getLastUpdate = async () => {
  return await db.collection(DB_LASTUPDATE).doc(DB_LASTUPDATE).get();
}

const replyWithText = async (replyToken, textMesg) => {
  return await request({
    method: `POST`,
    uri: `${LINE_MESSAGING_API}/reply`,
    headers: LINE_HEADER,
    body: JSON.stringify({
      replyToken: replyToken,
      messages: [
        {
          type: `text`,
          text: textMesg
        }
      ]
    })
  });
};


const orderTeam = async () => {
  const teamAll = await getTeamAll();
  const teamSort = {};
  teamAll.forEach((team) => {
    const data = team.data();
    const id = team.id;
    teamSort[id] = data;
  });
  return teamSort;
}

const checkDate = async () => {
  const lastUpdateAt = await getLastUpdate();
  const lastUpdateDate = lastUpdateAt.data().updateDate;
  const lastUpdateUTC = moment(lastUpdateDate.toDate()).utcOffset(7);
  const localTimeUTC = moment().utcOffset(7);
  console.log("LAST UPDATE ::", lastUpdateUTC);
  console.log("TODAY ::", localTimeUTC);
  if (!lastUpdateDate) {
    return false
  }
  return lastUpdateUTC.isSame(localTimeUTC, 'day');
}

const compareName = (a, b) => {
  const nameA = a.name.toUpperCase();
  const nameB = b.name.toUpperCase();
  if (nameA < nameB) {
    return -1;
  }
  if (nameA > nameB) {
    return 1;
  }
  return 0;
}

const comparePosition = (a, b) => {
  const positionA = a.position;
  const positionB = b.position;
  if (positionA < positionB) {
    return -1;
  }
  if (positionA > positionB) {
    return 1;
  }
  return 0;
}

const setUpMessage = (users, teams) => {
  for (const uid in users) {
    const user = users[uid];
    const teamData = teams[user.team];
    const value = teamData ? teamData : teams['other'];
    if (!value['data']) {
      value['data'] = [];
    }
    value['data'].push(user);
  }
  const result = []
  for (const key in teams) {
    const team = teams[key];
    const sortData = (team.data || []).sort(compareName)
    result.push({ title: team.title || 'Other', data: sortData, position: team.position })
  }
  const nResult = result.sort(comparePosition)
  let message = `Self Declared ${moment().utcOffset(7).format('DD/MM/YYYY')}\n`;
  let index = 0;
  let summary = 0;
  for (const key in nResult) {
    const element = nResult[key];
    message += `\n${element.title}\n`;
    for (const userObject of element.data) {
      message += `${userObject.status ? "✅" : "🅾️"} ${userObject.name}\n`;
      index += 1;
      if (userObject.status) {
        summary += 1;
      }
    }
  }
  message += `\n(${summary}/${index})`;
  if (summary === index) {
    message += '\n🎉🎉ครบทุกคนแล้วจ้า🎉🎉'
  }
  return message;
}

exports.callback = functions.region(region).https.onRequest(async (req, res) => {
  functions.logger.info("req.body", req.body);
  if (get(req, 'body.events[0].message.type') !== 'text') {
    return res.status(200).send("OK");
  } else {
    if (get(req, 'body.events[0].message.text') === '+1') {
      const isSameDay = await checkDate();
      console.log("CHECK DATE ::", isSameDay);
      if (!isSameDay) {
        await updateDate({
          updateDate: moment()
        });
      }
      const uid = req.body.events[0].source.userId;
      const dataAll = await getDataAll()
      const dataToPush = {}
      dataAll.forEach(async (element) => {
        const data = element.data();
        const id = element.id;
        dataToPush[id] = {
          name: data.name,
          status: uid === id ? true : isSameDay ? data.status : false,
          team: data.team
        };
        if (uid === id) {
          await updateData({
            id,
            params: {
              name: data.name,
              status: true,
              team: data.team
            }
          });
        } else if (!isSameDay) {
          await updateData({
            id,
            params: {
              name: data.name,
              status: false,
              team: data.team
            }
          });
        }
      });
      try {
        const teams = await orderTeam();
        const messageToPush = setUpMessage(dataToPush, teams);
        await replyWithText(req.body.events[0].replyToken, messageToPush);
      } catch (e) {
        console.log("Transform message => ", e.message)
      }

    }
  }
  return res.status(200).send("OK");
});